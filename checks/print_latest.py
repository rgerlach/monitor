# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
import requests
from requests_kerberos import HTTPKerberosAuth, DISABLED
import datetime
import pprint
import petl
import json
import sys

FALCON_HOST = "http://ip-54-40-237-124.huronc.merck.com:15000"
ENTITY_PATH = "/api/entities"
INSTANCE_PATH = "/api/instance"
PROCESS_INCLUDE = ['raw-bioassay-quick-fullingest-process','raw-bioassay-quick-fullindex-process','raw-dlc-quick-fullingest-process','raw-dlc-quick-fullindex-process', 'raw-content-eln-acl-fullingest-process']

def main(args=sys.argv):
    print args
    if len(args) > 1:
        start = args[1]
        end = args[2]
    else:
        start = str(datetime.date.today())
        start += "T00:00Z"
        end = str(datetime.date.today() + datetime.timedelta(days=1))
        end += "T00:00Z"

    #pp = pprint.PrettyPrinter(indent=4)
    print start
    payload = {"start": start, "end": end}
    for proc in PROCESS_INCLUDE:
        url = FALCON_HOST + INSTANCE_PATH + '/status/process/' + proc
        #print "Starting checks for Falcon jobs with %s " % url
        res = requests.get(url=url, params=payload, auth=HTTPKerberosAuth(mutual_authentication=DISABLED))
        #pp.pprint(res.text)
        print "###########    " + proc + "   ##########"
        if 'instances' in res.json():
            info = res.json()['instances'][0]
            info.pop('actions', None)
            info.pop('cluster', None)
            info.pop('details', None)
            infotable = petl.fromdicts([info, ])
            print petl.lookall(infotable)
            if 'actions' in res.json()['instances'][0]:
                table = petl.fromdicts(res.json()['instances'][0]['actions'])
                print petl.lookall(table)
        else:
            table = petl.fromdicts([res.json(), ]);
            print petl.lookall(table)

if __name__ == '__main__':
    main()
