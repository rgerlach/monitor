# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
import requests
from requests_kerberos import HTTPKerberosAuth, DISABLED
import datetime
import pprint
import petl
import json
import sys

FALCON_HOST = "http://ip-54-40-237-124.huronc.merck.com:15000"
INSTANCE_PATH = "/api/instance"

def main(args=sys.argv):
    print args
    if len(args) > 1:
        process = args[1]
        start = args[2]
        end = args[3]
    else:
        print "Usage <start_date> <end_date> <process_name>"
        sys.exit(1)

    pp = pprint.PrettyPrinter(indent=4)
    payload = {"start": start, "end": end}
    url = FALCON_HOST + INSTANCE_PATH + '/status/process/' + process
    res = requests.get(url=url, params=payload, auth=HTTPKerberosAuth(mutual_authentication=DISABLED))
    if 'instances' in res.json():
        for instance in res.json()['instances']:
            print "##### instance " + instance['instance'] + "  ######"
            actions = instance.pop('actions', None)
            inst_tbl = petl.fromdicts([instance, ])
            print petl.lookall(inst_tbl)
            if actions:
                act_tbl = petl.fromdicts(actions)
                print petl.lookall(act_tbl)

if __name__ == '__main__':
    main()
